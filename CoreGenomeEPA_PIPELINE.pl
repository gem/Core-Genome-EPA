#!usr/bin/perl

#pipeline first step: building the outpath structure and catching 

use Scalar::Util qw(looks_like_number);
use File::chdir;
#obtaine the location of the script
my $script = $0;
my @script_pipe = split /\//, $script;
pop @script_pipe;
my $script_pipe = join "/", @script_pipe;
use lib "scripts/";
use Functions;
my %opts;
&getopt(\%opts);
if(exists($opts{'h'})||(scalar(keys(%opts))==0)) 
{ 
	print "usage: $0 [options]\n";
	print " -h this message\n";
	print " -m input metagenomic dataset directory (REQUIRED)\n";
	print " -u Query Core Genome HMMsearch database (REQUIRED)\n";
	print " -d Close OUTGROUP HMMsearch database NAME or PATH (REQUIRED)\n";
	print "\tif the option NAME is used, the database is expected to\n\tbe placed in the same folder as the query database.\n\n";
	print " -a Core-gene Alignment FOLDER (REQUIRED)\n";
	print "\tAlignments must contain the sequences from all three \n\tcore genomes named by the lineages\n\n";
        print " -t Distant OUTGROUP HMMsearch database NAME or PATH (REQUIRED)\n";
	print "\tif the option NAME is used, the database is expected to\n\tbe placed in the same folder as the query database.\n\n";
        print " -o Output DIRECTORY or NAME where construct the output (REQUIRED)\n";
	print " -p Core Genome Phylogenetic tree (REQUIRED)\n";
	print " -s <value<=2> Minimum normalized score (Optional: Default 1)\n";
	print " -r <value<=2> Minimum Query/Outgroup ratio (Optional: Default 1)\n";
	print " -l Accepted List of References (Optional: Default All references)\n";
    exit;
}
chomp(my $pwd = qx/pwd/);
#first step: checking everything is in place:
my ($metagenomes,$query,$coutgroup,$doutgroup,$outpath,$tree,$score,$ratio,$alnfolder);
#if there are metagenomes?
if (!$opts{m}) {
	die "$0:\nUndefined or incorrect metagenomic dataset location. Aborting\n";
}else{
	$metagenomes = $opts{m};
	if ( -d $metagenomes) {
		
	}elsif(-d $pwd . "/" . $metagenomes){
		$metagenomes = $pwd . '/' . $metagenomes;
	}else{
		die "$0:\nUndefined or incorrect metagenomic dataset location. Aborting\n";
	}
}
#there is query database?
if (!$opts{u}) {
	die "\nUndefined or Missing Query Core Genome database. Aborting\n";
}else{
	$query = $opts{u};
	if (-e $query . '.h3i') {
		
	}elsif(-e $pwd . '/' . $query . '.h3i' ){
		$query = $pwd . '/'. $query;
	}else{
		die "\nUndefined or Missing Query Core Genome database. Aborting\n";
	}
	
}

#there are outgroup databases?
if (!$opts{d}) {
	die "\nUndefined or Missing close outgroup Core Genome databaase. Aborting\n";
}else{
	$coutgroup = $opts{'d'};
	my $querypath = director($query);
	if (-e $coutgroup . '.h3i') {
	}elsif(-e $pwd . '/' . $coutgroup){
		$coutgroup  = $pwd . '/' . $coutgroup;
	}elsif($querypath . $coutgroup . '.h3i'){
		$coutgroup = $querypath . '/'. $coutgroup;
	}else{
		die "\nUndefined or Missing close outgroup Core Genome databaase. Aborting\n";
	}
}
if (!$opts{t}) {
	die "\nUndefined or Missing distant outgroup Core Genome databaase. Aborting\n";
}else{
	$doutgroup = $opts{'t'};
	my $querypath;
	my @q = split /\//, $query;
	pop @q;
	$querypath = join '/', @q;
	if (-e $doutgroup . '.h3i') {
	}elsif(-e $pwd . '/' . $doutgroup){
		$doutgroup  = $pwd . '/' . $doutgroup;
	}elsif($querypath . $doutgroup . '.h3i'){
		$doutgroup = $querypath . '/'.  $doutgroup;
	}else{
		die "\nUndefined or Missing distant outgroup Core Genome databaase. Aborting\n";
	}
}

if (!$opts{a}) {
	die "\nUndefined or Missing Original alignment. Aborting\n";
}else{
	$alnfolder = $opts{'a'};
	if (-d $alnfolder) {
	}elsif(-d $pwd . '/' . $alnfolder){
		$tree  = $pwd . '/' . $alnfolder;
	}else{
		die "\nUndefined or Missing Original alignment. Aborting\n";
	}
}
#there is a phylogenetic tree?
if (!$opts{p}) {
	die "\nUndefined or Missing Phylogenetic tree. Aborting\n";
}else{
	$tree = $opts{'p'};
	if (-e $tree) {
	}elsif(-e $pwd . '/' . $tree){
		$tree  = $pwd . '/' . $tree;
	}else{
		die "\nUndefined or Missing Phylogenetic tree. Aborting\n";
	}
}
#there is an output folder?
if (!$opts{o}) {
	die 'Undefined Output directory or name. Aborting';
}else{
	$outpath = $opts{o};
	if ($outpath =~/^\//) {
		mkdir $outpath unless -d $outpath;
	}elsif($outpath =~/\//){
		$outpath = $pwd . '/' . $outpath;
		mkdir $outpath unless -d $outpath;
	}else{
		$outpath = $pwd . '/' . $outpath;
		mkdir $outpath unless -d $outpath;
	}
	die "Unable to create the Output directory and subdirectories. Maybe the path was incorrect? Aborting\n" if !-d $outpath;
}
#setting up the scores
if (!$opts{s}) {
	print "\nThe minimum score was not defined as number or undefined. Setting up to 1\n";
	$score = 1;
}elsif(looks_like_number($opts{s})){
	if ($opts{s}>=2) {
		$score = 2;
	}else{
		$score = $opts{s};
	}
	
}else{
	print "\nThe minimum score was not defined as number or undefined. Setting up to 1\n";
}

if (!$opts{r}) {
	print "\nThe minimum ratio was not defined as number or undefined. Setting up to 1\n";
	$score = 1;
}elsif(looks_like_number($opts{r})){
	if ($opts{r}>=2) {
		$ratio = 2;
	}else{
		$ratio = $opts{r};
	}
	
}else{
	print "\nThe minimum ratio was not defined as number or undefined. Setting up to 1\n";
}

my $accepted;
if (!$opts{l}) {
	$accepted = 'NULL';
}elsif(! -e $opts{l}){
	print "\nUndefined Reference File: Keeping all the original sequences\n";
	$accepted = 'NULL';
}else{
	open(ACC, $opts{l});
	while (<ACC>) {
		chomp;
		$accepted .= " " . $_ . " ";
	}
}


#check presence of softwares:


chomp(my @hmm = qx/which hmmsearch/);
my $hmm = $hmm[0];
undef @hmm;
die "$0: Unable to locate HMMsearch. Aborting\n" unless $hmm;
chomp(my @rax = qx/which raxmlHPC/);
my $rax = $rax[0];
undef @rax;
chomp(@rax = qx/which raxmlHPC-PTHREADS/);
my $tmp = $rax[0];
undef @rax;
my @mafft;
my $mafft;
chomp(@mafft = qx/which mafft-linsi/);
$mafft = $mafft[0];
die "$0: Unable to locate Mafft. Aborting\n" unless $mafft;
my $core;
if ($tmp) {
	$rax = $tmp;
	chomp(my @info = qx/sysctl -n hw.ncpu/);
	$core = $info[0];
	$core = 1 if !$core;
}elsif(!$rax){
	die "$0: Unable to locate Raxml. Aborting\n";
}

#Now, build the output guts

mkdir $outpath . '/input/'                     unless -d  $outpath .'/input_files/'         ;
mkdir $outpath . '/HMMoutput/'                 unless -d  $outpath .'/HMMoutput/'           ;
mkdir $outpath . '/selected_reads/'            unless -d  $outpath .'/selected_reads/'      ;
mkdir $outpath . '/input/original_alignments/' unless -d  $outpath .'/input/original_alignments/' ;
mkdir $outpath . '/input/phylogeny/'           unless -d  $outpath .'/input/phylogeny/' ;
mkdir $outpath . '/input/HMMdb/'               unless -d  $outpath .'/input/HMMdb/' ;
mkdir $outpath . '/alignments/'                unless -d  $outpath .'/alignments/'          ;
mkdir $outpath . '/alignments/logs/'           unless -d  $outpath .'/alignments/logs/'          ;
mkdir $outpath . '/aln_trimmed/'               unless -d  $outpath .'/aln_trimmed/'         ;
mkdir $outpath . '/aln_cleaned/'               unless -d  $outpath .'/aln_cleaned/'         ;

#copy things in the correct places unless first step is done;
#check number of files in the output and number of files in the input;
opendir MTG, $metagenomes;
my @files = grep !/^\./, readdir MTG;
closedir MTG;
my ($n_files_input,$n_files_output) = Functions::nfile_checker($metagenomes,"$outpath/HMMoutput/");
#copy the input files into a good location
	my @tree=split /\//, $tree;
	my $tree2=pop @tree;
	$tree2 = "$outpath/input/phylogeny/$tree2";
	qx/cp -r $tree $outpath\/input\/phylogeny\// unless -e $tree2;
	$tree = $tree2;
do{
	qx/cp -r $alnfolder\/* $outpath\/input\/original_alignments/;
	my($n_files_input,$n_files_output) = Functions::nfile_checker($query,"$outpath\/input\/HMMdb\/");
	qx/cp -r $query* $outpath\/input\/HMMdb\// unless $n_files_input == $n_files_output;
	
	($n_files_input,$n_files_output) = Functions::nfile_checker($coutgroup,"$outpath\/input\/HMMdb\/");
	qx/cp $coutgroup* $outpath\/input\/HMMdb\//unless $n_files_input == $n_files_output;
	
	($n_files_input,$n_files_output) = Functions::nfile_checker($doutgroup,"$outpath\/input\/HMMdb\/");
	qx/cp $doutgroup* $outpath\/input\/HMMdb\//unless $n_files_input == $n_files_output;
	
}if($n_files_output < 3*$n_files_input);
$query = namer($query);
$coutgroup = namer($coutgroup);
$doutgroup = namer($doutgroup);
#first step: Calling HMMsearch
opendir MTG, $metagenomes;
@files = grep !/^\./, readdir MTG;
closedir MTG;
while (@files) {
	my $file_id = shift @files;
	my $file = $metagenomes . $file_id;
	$file_id =~s/\.[^\.]+$//;
	my $string = $hmm . " -o /dev/null --domtblout $outpath/HMMoutput/$file_id\.$query\.out ";
	$string .= "--cpu $core" if $core>1;
	$string .= " $outpath/input/HMMdb/$query $file";
	qx/$string/ unless -e "$outpath/HMMoutput/$file_id\.$query\.out";
	undef $string;
	$string = $hmm . " -o /dev/null --domtblout $outpath/HMMoutput/$file_id\.$coutgroup\.out ";
	$string .= "--cpu $core" if $core>1;
	$string .= " $outpath/input/HMMdb/$coutgroup $file";
	qx/$string/ unless -e "$outpath/HMMoutput/$file_id\.$coutgroup\.out";
	undef $string;
	$string = $hmm . " -o /dev/null --domtblout $outpath/HMMoutput/$file_id\.$doutgroup\.out ";
	$string .= "--cpu $core" if $core>1;
	$string .= " $outpath/input/HMMdb/$doutgroup $file";
	qx/$string/ unless -e "$outpath/HMMoutput/$file_id\.$doutgroup\.out";
	#Metagenome file is done. now parsing HMMresults into a single file
}

my $wow;
#second step: selecting sequences with good values and extracting them from the metagenome dataset
my $error = Functions::mother($query,$coutgroup,$doutgroup,"$outpath/HMMoutput/",$metagenomes,"$outpath/selected_reads/",$score,$ratio,$core/4);
my @reads;
if ($error == -1) {
	#check whether joining step has been performed already
	opendir DIR, "$outpath/selected_reads/";
	my @files = grep /\.hmmout/, readdir DIR;
	my $scalar = scalar @files;
	undef @files;
	closedir DIR;
	
	opendir JOIN, "$outpath/selected_reads/joined/";
	@reads = grep /\.fa/, readdir JOIN;
	my $scalar_join = scalar @files;
	closedir JOIN;
	Functions::joiner("$outpath/selected_reads/") unless $scalar == $scalar_join;
	
}

Functions::error($error);
#third step: performing the alignments
	#define references;
my @refs;
#check whether part three is done
undef $n_files_input;
undef $n_files_output;
($n_files_input,$n_files_output) = Functions::nfile_checker("$outpath/selected_reads/joined/","$outpath/alignments/");
#rename original alignments
my $err = Functions::OriAlnRenamer($outpath . '/input/original_alignments/');
Functions::error($err);
if($n_files_input == $n_files_output){
	opendir JOIN, "$outpath/selected_reads/joined/";
	@reads = grep /\.fa/, readdir JOIN;
	closedir JOIN;
	my $id = shift @reads;
	$id=~s/\.fa//;
	my $ref_aln = $outpath . '/input/original_alignments/' . $id . '.original.aln';
	open(REF, $ref_aln);
	while (<REF>) {
		if (/>(\S+)/) {
			my $tmp = $1;
			$tmp=$1 if $tmp=~/^(\S{4}\d{3})/;
			push @refs, $tmp;
		}
	}
	close REF;
}else{
	opendir JOIN, "$outpath/selected_reads/joined/";
	@reads = grep /\.fa/, readdir JOIN;
	closedir JOIN;
	mkdir $outpath . "/input/alignments_renamed/" unless -d "$outpath/input/alignments_renamed/";
	while (@reads) {
		my $id = shift @reads;
		my $file = $outpath . "selected_reads/joined/" . $id;
		$id=~s/\.fa//;
		my $ref_aln = $outpath . '/input/original_alignments/' . $id . '.original.aln';
		my $ref_out = $outpath . '/input/alignments_renamed/' . $id . '.renamed.aln';
		Functions::RmIfEmpty($ref_out);
		#fill reference and rename alignments;
		if (scalar @refs == 0) {
			open(REF, $ref_aln);
			open(REFOUT,">", $ref_out) unless -e $ref_out;
			while (<REF>) {
				if (/>(\S+)/) {
					my $tmp = $1;
					$tmp=$1 if $tmp=~/^(\S{4}\d{3})/;
					push @refs, $tmp;
					print REFOUT ">" . $tmp . "\n";
				}else{
					print REFOUT $_;
				}
			}
			close REF;
			close REFOUT;
		}else{
			open(REF, $ref_aln) unless -e $ref_out;
			open(REFOUT,">", $ref_out) unless -e $ref_out;
			while (<REF>) {
				if (/>(\S+)/) {
					my $tmp = $1;
					$tmp=$1 if $tmp=~/^(\S{4}\d{3})/;
					print REFOUT ">" . $tmp . "\n";
				}else{
					print REFOUT $_;
				}
			}
			close REF;
			close REFOUT;
		}
		my $out = $outpath . '/alignments/' . $id . ".addfragments.aln";
		my $log = $outpath . '/alignments/logs/' . $id . ".mafft.log";
		Functions::RmIfEmpty($out);
		my $command = $mafft . " --thread $core --addfragments $file $ref_out > $out 2>$log";
		qx/$command/ unless -e $out;
	}	
};

my $ref = join " ", @refs;

#fourth step: applying Filters and calling raxml
mkdir $outpath . 'raxout/' unless -d $outpath . "raxout/";
undef @files;
	
	opendir ALN, $outpath . '/alignments/';
	@files = grep !/^\./ && /\.addfragments\.aln/, readdir ALN;
	my @out1 = @files;
	my @out2 = @files;
	$_ = $outpath . '/alignments/' . $_ for @files;
	$_ = $outpath . '/aln_trimmed/' . $_ for @out1;
	$_=~s/\.aln/\.trimmed\.aln/ for @out1;
	$_ = $outpath . '/aln_cleaned/' . $_ for @out2;
	$_=~s/\.aln/\.cleaned\.aln/ for @out2;
	closedir ALN;
	while (@files) {
		my $in = shift @files;
		my $out = shift @out1;
		my $out2 = shift @out2;
		my $num = $1 if $in=~/(\d+)\.addfragments/;
		my $out3 = $outpath . '/raxout/RAxML_info.raxml_' . $num;
		#First filter: gappyout;
		Functions::RmIfEmpty($out);
		Functions::RefGapTrimmer($in,$out,$ref,$accepted) unless -e $out;
		my $nseq = Functions::DistSeqTrimmer($out,$out2,$ref,$accepted,$tree) unless -e $out2;
		next if -e $out3;
		#and NOW we perform raxML per file
		my $model = Functions::DefineModel($out2);
		$CWD = $outpath . "/raxout/";
		my $command = $rax . " -s " . $out2 . " -t " . $tree . " -T " . $core . " -f v -m" . $model . " -n raxml_" . $num;
		qx/$command/ unless $nseq == 0;
		$CWD = $script_pipe;
	}
	
	#second filter: distance;



exit;

sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}

sub namer{
	my ($path) = @_;
	my @path = split /\//, $path;
	my $name = pop @path;;
	return $name;
}
sub director{
	my ($path) = @_;
	my @path = split /\//, $path;
	pop @path;
	my $name = join '/', @path;
	return $name;
}
sub nfile_checker{
	my($in,$out) = @_;
	my @in = split /\//, $in;
	pop @in;
	my $in_short = join "/", @in;
	#check if -d $in
	if (-d $in) {
		opendir IN, $in;
	}elsif(-d $in_short){
		opendir IN, $in_short;
	}
	undef @in;
	opendir OUT,$out;
	@in = grep !/^\./, readdir IN;
	my @out = grep !/^\./, readdir OUT;
	closedir IN;
	closedir OUT;
	return(scalar(@in),scalar(@out));
	
}