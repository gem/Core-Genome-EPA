#!usr/bin/perl

use strict;

my $path = '/Volumes/biggem/Users/mgarcia/data/Klebsiella_metagen/Blind_test/msa_with_fragments/';
opendir PATH, $path;
my @files = grep /.aln/, readdir PATH;
closedir PATH;

while(@files){
    my $id = shift @files;
    my $file = $path . $id;
    open FILE, $file;
    my @file =<FILE>;
    close FILE;
    open OUT, ">", $file;
    while(@file){
        my $line = shift @file;
        if($line=~/^>([^\_]+)\_\d+\s/){
            $line = $1 . "\n";
        }elsif($line=~/^\S{4}\d{3}\w\d{2}\w\n/){
            $line = ">" . $line;
        }
        print OUT $line;
    }
}