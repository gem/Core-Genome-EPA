#!usr/bin/perl

use strict;


chomp(my $path = $ARGV[0]);
chomp(my $alnpath = $ARGV[1]);

die unless -d $path;
die unless -d $alnpath;

opendir PATH, $path;
my @files = grep /\.fna/, readdir PATH;
closedir PATH;

while(@files){
    my $id = shift @files;
    my $dir = $path . $id;
    my $profile = $1 if $id=~/selected\.(\d+)\.fna/;
    my $aln = $alnpath . 'gene_' . $profile . ".trimmed";
    my $query = 'mafft-linsi --thread -1 --add ' . $dir . " " . $aln . " > " . $path . 'selected.' . $profile . '.aln';
    qx/$query/;
}