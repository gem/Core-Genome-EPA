#!usr/bin/perl

use strict;
#chomp(my $folder = $ARGV[0]);
my $folder = '/Volumes/biggem/Users/mgarcia/data/Klebsiella_metagen/alignments_w_fragments/';
mkdir $folder . "dirty/" unless -d $folder . 'dirty/';
mkdir $folder . 'clean/' unless -d $folder . 'clean/';
opendir FOLDER, $folder;
opendir FOLDER2, $folder;
my @files = grep /\.aln/, readdir FOLDER;
push @files, grep /\.mafft/, readdir FOLDER2;
closedir FOLDER2;
closedir FOLDER;
my @files2 = @files;

my %finals;
while(@files){
    my $id = shift @files;
    my $file = $folder . $id;
    #first phase:Alignment pars and clean
    $id = $1 if $id=~/^(\d+)\./;
    print $id . "\n";
    my $out1 = $folder . "clean/" . $id . 'w.fragments.mafft';
    my $out2 = $folder . "dirty/" . $id . ".w.fragments.mafft";
    my %reads;
    my %refs;
    open(FILE, $file);
    my ($name,$type);
    open(TMP, ">", $folder . 'tmp.aln');
    while (<FILE>) {
        if (/>(\S+)/) {
            $name = $1;
            if ($name=~/^(\d{7})/) {
                $type = 'frag';
            }else{
                $type = 'ref';
                print TMP $_;
            }
        }else{
            if ($type eq 'ref') {
                print TMP $_;
            }
            chomp;
            $reads{$type}{$name} .= $_;
        }
        
    }
    close FILE;
    close TMP;
    my @output = qx/trimal -in $folder\/tmp.aln -out \/dev\/null -noallgaps -colnumbering/;
    qx/rm $folder\/tmp.aln/;
    @output = split /\,\s/, $output[1];
    open(OUT1, ">", $folder . 'dirty/' . $id .'.no_ref_gaps.msa');
    my @keys1 = sort {$a cmp $b} keys %{$reads{ref}};
    while (@keys1) {
        my $key = shift @keys1;
        my @aln = split "", $reads{ref}{$key};
        my @out = @output;
        my $outseq;
        while (@out) {
            my $pos = shift @out;
            $outseq .= $aln[$pos];
        }
        $reads{ref}{$key} = $outseq;
        print OUT1 ">" . $key . "\n" . $outseq . "\n";
    }
    my @keys2 = sort {$a cmp $b} keys %{$reads{frag}};
    while (@keys2) {
        my $key = shift @keys2;
        my @aln = split "", $reads{frag}{$key};
        my @out = @output;
        my $outseq;
        while (@out) {
            my $pos = shift @out;
            $outseq .= $aln[$pos];
        }
        my $tmp = $outseq;
        $tmp=~s/\-//g;
        next if length($tmp)<25;
        $reads{ref}{$key} = $outseq;
        print OUT1 ">" . $key . "\n" . $outseq . "\n";
    }
    close OUT1;
    #second phase:Read removal by distance
    #final phase: Concat
}