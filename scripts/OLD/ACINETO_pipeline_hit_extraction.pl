#!usr/bin/perl

chomp(my $dir = $ARGV[0]);
chomp(my $other_dir = $ARGV[1]);
chomp(my $outdir = $ARGV[2]);
opendir DIR, $dir;
my @files = grep /\.lst/, readdir DIR;
closedir DIR;

while (@files) {
    my $id = shift @files;
    my $out = $dir . $id;
    $id=~s/\.lst//;
    my $tmpdir = $dir . $id . "/";
    my $fasta = $other_dir . $id .".550.cluster.aa90.faa";
    my $string = "getentryX -f $out $fasta";
    qx/getentryX -f $out $fasta/;
    qx/mv *.fa $tmpdir\//;
    opendir DIR, $tmpdir;
    my @tmps = grep /\.fa/, readdir DIR;
    closedir DIR;
    while (@tmps) {
        my $tmpid = shift @tmps;
        
        my $tmp = $tmpdir . $tmpid;
        open(F, $tmp);
        open(OUT,">>", $outdir . $tmpid);
        $tmpid=~s/\.fa//;
        while (<F>) {
            my $line = $_;
            if ($line=~/>(\S+)/) {
                my $tmp = $1;
                my $profile = $tmpid;
                $line=~s/>/>$id\_/;
            }
            print OUT $line;
        }
        close F;
        close OUT;
    }
}
