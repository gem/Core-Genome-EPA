#!usr/bin/perl

use strict;

my %opts;
&getopt(\%opts);

if(exists($opts{'h'})||(scalar(keys(%opts))==0)) 
    #HELP OUTPUT
{ 
	print "usage: $0 [options]\n";
        print " -a Reference selection to calculate distance (optional)\n";
	print " -h this message\n";
        print " -m Matrix (REQUIRED)\n";
        print " -o Output Folder (optional)\n";
	print " -p MSA Folder (REQUIRED)\n";
        print " -r Reference names (REQUIRED)\n";
        print " -1234 Reference species to calculate the maximum distance (optional. One number each)\n";
    exit;
}
my $matrix = $opts{'m'};
if($matrix !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $matrix = $pwd . "/" . $matrix;
}
die "$0: No matrix defined. Aborting\n" if ! -e $matrix;
my %matrix = &matrix_extractor($matrix);

my $ref_file = $opts{'r'};
if($ref_file !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $ref_file = $pwd . "/" . $ref_file;
}
die "$0: Ref File does not exist. Aborting\n" unless -e $ref_file;
open(REF, $ref_file);
my $ref;
while (<REF>) {
    chomp;
    $ref .= " " . $_ . " ";
}
close REF;
my $acc;
my $accepted = $opts{'a'};
if(!$accepted){

}elsif($accepted !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $accepted = $pwd . "/" . $accepted;
}
if (-e $accepted) {
    open(ACC, $accepted);
    while (<ACC>) {
       chomp;
       $acc .= " " . $_ . " ";
    }
    close ACC;
}else{
    $acc = $ref;
}
my $path = $opts{'p'};
if($path !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $path = $pwd . "/" . $path;
}

die "$0: This directory does not exist. Aborting\n" unless -d $path;

my $outpath = $opts{'o'};

if (!$opts{'o'}) {
    $outpath = $path . 'output/';
    mkdir $outpath unless -d $outpath;
}else{
    if($outpath !~/^\//){
        chomp(my $pwd = qx/pwd/);
        $outpath = $pwd . "/" . $outpath;
    }
    if (!-d $outpath) {
        mkdir $outpath unless -d $outpath;
    }
}
$outpath .= "/";
$path .= "/";
my ($one,$two,$three,$four);
if ($opts{1}) {
    $one = $opts{1};
    undef $one unless $ref =~/$one/;
}
if ($opts{2}) {
    $two = $opts{2};
    undef $two unless $ref =~/$two/;
}
if ($opts{3}) {
    $three = $opts{3};
    undef $three unless $ref =~/$three/;
}
if ($opts{4}) {
    $four = $opts{4};
    undef $four unless $ref =~/$four/;
}


opendir PATH, $path;
my @files = grep /\.aln/||/\.msa/, readdir PATH;
closedir PATH;

while (@files) {
    my $id = shift @files;
    my $file = $path . $id;
    $id=~s/\.[^\.]+$//;
    print "\t" . $id . "\n";
    my $out = $outpath . $id . ".wo.outliers.aln";
    next if -e $out;
    open(OUT,">",$out);
    open(FILE, $file);
    my %data;
    my ($name,$type);
    while (<FILE>) {
        chomp;
        if (/>(\S+)/) {
            $name = $1;
            if ($ref=~/\s$name\s/) {
                $type = 'ref';
            }else{
                $type = 'seq';
            }
        }else{
            if ($type eq 'ref') {
                $data{ref}{$name}.=$_;
            }else{
                $data{seq}{$name}.=$_;
            }
        }
    }
    close FILE;
    my $max = 0;

    my %dist;
    my @keys = sort {$a cmp $b} keys %{$data{ref}};
    while(@keys){
        my $key = shift @keys;
        my $seq = $data{ref}{$key};
        print OUT ">" . $key . "\n" . join("\n",unpack("(A60)*", $seq)) . "\n"; 
    }
    $acc=~s/\s+/ /g;
    $acc=$1 if $acc=~/^\s(.*)\s$/;
    @keys = split " ", $acc;
    my @keys2 = @keys;
    do{
        while (@keys) {
            my $key1 = shift @keys;
            my $length = length $data{ref}{$key1};
            if (!$length) {
                   next;
            }
            my @ks = @keys;
            while (@ks) {
                my $key2 = shift @ks;
                $dist{$key1}{$key2} = 0 if $key1 eq $key2;
                next if $key1 eq $key2;
                $dist{$key1}{$key2} = $dist{$key2}{$key1} if $dist{$key2}{$key1} >0;
                next if $dist{$key1} && $dist{$key1}{$key2} && $dist{$key2}{$key1} == $dist{$key1}{$key2};
                my @aa1 = split "", $data{ref}{$key1};
                my @aa2 = split "", $data{ref}{$key2};
                my $gapp = 3;
                while (@aa1) {
                    my $aa1 = shift @aa1;
                    my $aa2 = shift @aa2;
                    next if $aa1 eq $aa2;
                    my $penalty;
                    if ($aa1 ne '-' && $aa2 ne '-') {
                        $penalty = abs($matrix{$aa1}{$aa2});
                        $gapp = 3;
                    }else{
                        $penalty = $gapp;
                        $gapp++;
                    }
                    $dist{$key1}{$key2} += $penalty;
                }
                
                
                $dist{$key1}{$key2} /= $length;
                if ($max < $dist{$key1}{$key2}) {
                    $max = $dist{$key1}{$key2};
                    $one = $key1;
                    $two = $key2;
                }
                $dist{$key2}{$key1} = $dist{$key1}{$key2};
            }
        }
        my @new_keys;
        @new_keys = sort {$dist{$one}{$b} <=> $dist{$one}{$a}} keys %{$dist{$one}};
        if (scalar keys %{$data{ref}}>=3) {
            do{
                $three = shift @new_keys;
            }until($three ne $two);
            @new_keys = sort {$dist{$three}{$b} <=> $dist{$three}{$a}} keys %{$dist{$three}};
            if (scalar keys %{$data{ref}}>=4) {
                do{
                    $four = shift @new_keys;
                }until($four ne $three && $four ne $two && $four ne $one);
            }
            
            
        }
        
        
        
        undef %dist;    
    }unless(defined $one && defined $two && defined $three && defined $four);
    @keys = sort {$a cmp $b} keys %{$data{seq}};    
    while (@keys) {
        my $key = shift @keys;
        my $seq = $data{seq}{$key};
        my ($left,$sseq,$right);
        if ($seq=~/^[^\-]/&&$seq=~/[^\-]$/) {
            ($sseq) = ($seq);
        }elsif($seq=~/^[^\-]/){
            ($left,$sseq,$right) = (0,$1,length($2)) if $seq=~/^(\S+[^-])([-]+)$/;
        }elsif($seq=~/[^\-]$/){
            ($left,$sseq) = (length($1),$2) if $seq=~/^([-]+)([^\-]\S+)$/;
        }else{
           ($left,$sseq,$right) = (length($1),$2,length($3)) if $seq=~/^([-]+)([^\-].*[^\-])([-]+)$/;
        }
        my $length = length($sseq);
        my @sseq;
        my @ref;
        push @sseq, substr($data{ref}{$one},$left,$length);
        push @ref, substr($data{ref}{$one},$left,$length);
        push @sseq, substr($data{ref}{$two},$left,$length);
        push @ref, substr($data{ref}{$two},$left,$length);
        push @sseq, substr($data{ref}{$three},$left,$length);
        push @ref, substr($data{ref}{$three},$left,$length);
        push @sseq, substr($data{ref}{$four},$left,$length);
        push @ref, substr($data{ref}{$four},$left,$length);
        my $min_dist = 0;
        my @sseq_tmp = @sseq;
        while (@sseq) {
            my $sseq1 = shift @sseq;
            my @sseq2 = @sseq;
            
            while (@sseq2) {
                my $sseq2 = shift @sseq2;
                my @aa1 = split "", $sseq1;
                my @aa2 = split "", $sseq2;
                my $dist;
                while (@aa1) {
                    my $aa1 = shift @aa1;
                    my $aa2 = shift @aa2;
                    next if $aa1 eq $aa2;
                    my $penalty;
                    my $gapp = 3;
                    if ($aa1 ne '-' && $aa2 ne '-') {
                        $penalty = abs($matrix{$aa1}{$aa2});
                        $gapp = 3;
                    }else{
                        $penalty = $gapp;
                        $gapp++;
                    }
                    $dist += $penalty;
                }
                if (!$length) {
                    my $wow;
                }
                
                $dist /= $length;
                if ($min_dist < $dist) {
                    $min_dist = $dist;
                }
            }
        }
        my $dist = 1;
        while (@sseq_tmp) {
           my @aa1 = split "", $sseq;
           my @aa2 = split "", shift @sseq_tmp;
           my $tmp = 0;
            while (@aa1) {
                my $aa1 = shift @aa1;
                my $aa2 = shift @aa2;
                next if $aa1 eq $aa2;
                my $penalty;
                my $gapp = 3;
                if ($aa1 ne '-' && $aa2 ne '-') {
                    $penalty = abs($matrix{$aa1}{$aa2});
                    $gapp = 3;
                }else{
                    $penalty = $gapp;
                    $gapp++;
                }
                $tmp += $penalty;
            }
            $tmp /= $length;
            if ($tmp<$dist) {
                $dist = $tmp;
            }
        }
        if ($dist<=$min_dist) {
            print OUT ">" . $key . "\n" . join("\n",unpack("(A60)*",$data{seq}{$key})) . "\n";
        }else{
            my $wow;
        }
    }
    close OUT;
}


exit;

sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}


sub empty_value{    
    our($args,%opts)=@_;
    my ($return,$val,@keys);
    if($args eq 'bi'){
        $keys[0]='bi';
    }else{
        @keys=split '',$args;
    }
    foreach (@keys) {
        if(!defined($opts{$_})){
            $val=$_;
            if ($val eq 'p'){
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    system('stty','-echo');
                    chop(my $value=<STDIN>); #hide the value for password
                    system('stty','echo');
                    
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                    }
            
                }    
            }else{
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    my $value=<STDIN>;
                    chomp($value);
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                        }
            
                }
            }
            if(!defined($opts{$_})){$return=$val;return $return;}
        } 
    }
    $return=0;
    return ($return);
}

sub matrix_extractor{
    my ($f) = @_;
    open(F, $f);
    my @pos;
    my %m;
    while (<F>) {
        chomp;
        next if /^#/;
        if (/^\s/) {
            @pos = split " ", $_;
            #shift @pos;
        }else{
            my @line = split " ", $_;
            my $aa = shift @line;
	    next if $aa eq '*';
            for(my $i = 0;$i<scalar @line; $i++){
                $m{$pos[$i]}{$aa} = $line[$i];
            }
        }
        
    }
    return %m;
}