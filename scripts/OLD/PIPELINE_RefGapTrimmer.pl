#!usr/bin/perl

use strict;

chomp(my ($file, $ref) = @ARGV);
die unless -e $file;
#I open the Reference List File
open REF, $ref;
#I put the information in a variable array
chomp(my @list = <REF>);

close REF;

#Subroutine to load sequence data into variable type HASH
my %seqs = &parser_fasta($file);

#Now I read the keys from the hash and start doing things with them (If you know what I mean *DirtyFace*)
my @keys = keys %seqs;
my @out;
my $ln;
#I start removing from list (aka, I start doing things with the Reference List)
my @ref = @list; # I keep the list just in case
while(@list){
    #foreach element on List
    my $ref = shift @list;
    #I select the sequence associated to the element on list 
    my @seqid = grep /^$ref/, @keys;
    my $seqid = $seqid[0];
    my $seq = $seqs{$seqid};
    #I split the sequence in monomers
    my @seq = split "", $seq;
    $ln = scalar @seq;
    my $i = 0;
    #I create an array (@out), in which if there isn't a gap in that sequence, that position does not change (1).
    #But if it is a gap, the position will be considered as empty and will be removed
    while (@seq) {
        my $pos = shift @seq;
        if ($pos eq '-') {
            $out[$i] = 0 unless $out[$i]==1;
        }else{
            $out[$i] = 1;
        }
        $i++;
    }
    
    
}
#now I start with ALL the sequences and I remove the positions that are all gaps.
my @keys = sort {$b cmp $a } @keys;
while (@keys) {
    my $key = shift @keys;
    my $seq = $seqs{$key};
    if ($key=~/\:/) {
        $key=~s/\:/\_/g;
    }else{
        my $totoro = &multiregex($key,@ref);
        do { $key = $1 if $key=~/^(\S{4}\d{3}\S\d{2}\S)/ } if $totoro ==1;
        
    }
    print ">" . $key . "\n";
    my @seq = split "", $seq;
    for(my $i = 0; $i<$ln;$i++){
        if ($out[$i] == 0) {
            #code
        }else{
            print $seq[$i];
        }
    }
    print "\n";
}




exit;
sub multiregex{
    my ($toto,@cucu) = @_;
    my $result = 0;
    while (@cucu) {
        my $cucu = shift @cucu;
        if ($toto=~/$cucu/) {
            $result = 1;
            undef @cucu;
        }
    }
    return $result;
}
sub parser_fasta{
    my ($in) = @_;
    my %out;
    open IN, $in;
    my $name;
    while(<IN>){
        chomp;
        if(/>(\S+)/){
            $name = $1;
        }else{
            $out{$name} .= $_;
        }
    }
    close IN;
    return %out;
}