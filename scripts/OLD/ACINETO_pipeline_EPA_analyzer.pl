#!usr/bin/perl

use strict;
use Cwd;
our %opts;
&getopt(\%opts);

if(exists($opts{'h'})||(scalar(keys(%opts))==0)) 
    #HELP OUTPUT
{ 
	print "usage: $0 [options]\n";
	print " -p EPA Folder (REQUIRED)\n";
        print " -r Reference names (REQUIRED)\n";
	print " -o Output Folder (optional)\n";
	print ' -m {1|2|3} Method of assignation (Optional). Default == 3' . "\n";
	print "\t -m 1 Use Best Hit in EPA\n";
	print "\t -m 2 Use Probabilistic results from EPA assignations\n";
	print "\t -m 3 Use both methods and retrieve separated output results\n";
    exit;
}
#checks if every command is correct and the folders/files do exist
my $ref_file = $opts{r};
die 'undefined Reference File. Aborting\n.' unless $ref_file;
if ($ref_file!~/^\//) {
	chomp(my $pwd = getcwd);
	$ref_file  = $pwd . $ref_file;
}
die 'Unknown reference file or incorrect location. Aborting\n' unless -e $ref_file;
my $inpath = $opts{p};
die 'undefined Input Evolutionary Placement Output Folder. Aborting\n.' unless $inpath;
if ($inpath!~/^\//) {
	chomp(my $pwd = getcwd);
	$inpath  = $pwd . $inpath . "/";
}
die 'Unknown EPA directory or incorrect location. Aborting\n' unless -d $inpath;
my $outpath = $opts{o};
if (!$outpath){
	my @outpath = split /\//, $inpath;
	pop @outpath;
	push @outpath, 'EPA_output/';
	$outpath = join "/", @outpath;
	mkdir $outpath unless -d $outpath;
}elsif ($outpath!~/^\//) {
	chomp(my $pwd = getcwd);
	my @outpath = split /\//, $outpath;
	my $tmp = $pwd;
	while (@outpath) {
		my $position = shift @outpath;
		$tmp .= "/" . $position;
		if (! -d $tmp) {
			mkdir $tmp;
		}
	}
}
print "Outputs will be placed in " . $outpath . "\nProceeding....";
open(REF, $ref_file);
my @core;
chomp(@core = <REF>);
my $method = $opts{m};
if (!$method) {
	$method = $3;
}
#opens and loads in memeory a list of json files (aka jplace, according to pplacer and epa)
opendir PATH, $inpath;
my @files = grep /\.jplace/, readdir PATH;
closedir PATH;
#here we save the good results: 5 vars: read, metagenome, node, weight, direct assignation and probability
#direct assignation means not scatter placement
my %results;
#names of the metagenomes
my $metagenomes;
#absolute counts
my %freqs;
my %freqs2;
#the probabilities analysis. It is dropped now.
my %probabilities_adj;
my %probabilities;
#good sequences: Those that the best score is > 0.5 ELW
my %good;
#Names of tips
my @leave_ids;
#number of nodes
my $max_node;
while (@files) {
	# I collect the profile Id
	my $id = shift @files;
        my $profile = $1 if $id =~/\D(\d+)[\_\.]\S+\.jplace/;
        print $profile . "\n";

	my $file = $inpath . $id;
        my %info;
	#I START reading the file. This file contains 2 parts:
		#1) The tree
		#2) The assignations.
        open(FILE, $file);
	while (<FILE>) {
            chomp;
	    #I save the tree in a variable to unstruct it and analyze the lineages from tip to root
            if (/\"tree\"\:\s\"(.*)\"/) {
                $info{tree} = $1;
	    #I start saving the placements. Placements have the following structure:
	    #{"p"[node,likelihood,ELW,branch lenght, position from closest node] this might come multiple times. I set the max to the top 5% scores.; "n"[read_name]}
	    #I extract everything and I collect it in a hash {%info}
            }elsif(/\"p\"\:\[\[(.*)\]\],\s\"n\":\[\"(.*)\"\]/){
                my @p = split /\]\,\[/, $1;
                my $n = $2;
		if ($n eq '4450243_GD7ZOOC02F2K00_47_455_-') {
			my $wow;
		}
                my $n2 = $1 if $n=~/^(.*)\_\d+$/;
                #next if $list !~/$n2/;
                my $p = $p[0];
                
                $p=~s/\s//g;
                    my @sp = split /\,/, $p;
                    $info{assigned}{$n}{node} = $sp[0];
		    if ($info{assigned}{$n}{node}>$max_node) {
			$max_node = $info{assigned}{$n}{node};
		    }
                    $info{assigned}{$n}{likelihood} = $sp[1];
                    $info{assigned}{$n}{weight} = $sp[2];
                    $info{assigned}{$n}{length} = $sp[3];
                    $info{assigned}{$n}{pendant} = $sp[4];
		    
                my $i = 0; 
                while (@p) {
                    my $p = shift @p;
                    $p=~s/\s//g;
                    my @sp = split /\,/, $p;
                    $info{ass}{$n}{$i}{node} = $sp[0];
                    $info{ass}{$n}{$i}{likelihood} = $sp[1];
                    $info{ass}{$n}{$i}{weight} = $sp[2];
                    $info{ass}{$n}{$i}{length} = $sp[3];
                    $info{ass}{$n}{$i}{pendant} = $sp[4];
                    $i++;
                }
                
            }
        }
	#now, I start deconstructing the tree. I build a bunch of variables to store tips, nodes, and branch lengths.
	my $tree = $info{tree};
        
        my %tree;
        my %ids;
        my @nodes;
        my %nodes;
        my %dist;
        my $line = "\|";
        my ($open,$i)=(0,0);
        my @close;
        
        my @tree = split "", $tree;
        my @coma = split ",", $tree;
        my @temp_names;
        
        my $limit;
        my %leaves;
        my %taxa;
        my %distances;
        my %bootstrap;
        my %shorter;
        my @id_names;
        #check for rooting;
        #Check for equal sequence names
        my %id_leaves;
        my %id_nodes;
        my @open;
        my %node_leaves;
        $i = 0;
        my @leaves;
	#In brief what this part does, is to collect each node and see which branches descend from him (directly and indirectly).
	#Each branch is defined by the starting node and the ending node.
	#Then, I have the annotations from  
        foreach(@coma){
            ~s/\)/\\\)/g;
            if(/\(/){
                do{
                    if(/^\(\(/){
                        push @nodes, $i;
                    }elsif(/^\(([^\:]+)\:(\d+\.\d+e\-\d+|\d+\.\d+)\{(\d+)\}/){ 
                        push @nodes, $i;
			my $id = $1;
                        my $value =  $2;
                        my $node = $3;
                        $leaves{$1} = $value;
                        $id_leaves{$node} = $1;
                        @nodes = sort {$a<=>$b} @nodes;
                        my $line = join ",", @nodes;
                        foreach(@nodes){
                            $node_leaves{$_} .= ",". $1  ;
                        }
                        $taxa{$1} = $line;
                        $_=~s/$1\:$2\{$node\}//;              
                    }
                    $i++;
                    $_=~s/\(//;
                }while(/\(/);
            }
            if(/\)[\w]{0,3}:(\d+\.\d+e\-\d+|\d+\.\d+)/){
                do{
                    if(/^([^\:\)]+)\:(\d+\.\d+)\{(\d+)\}/){
                        my ($id,$dist,$node) = ($1,$2,$3);
                        my $value =  $dist;
                        $leaves{$1} = $value;
                        @nodes = sort {$a<=>$b} @nodes;
                        my $line = join ",", @nodes;
                        foreach(@nodes){
                            $node_leaves{$_} .= ",". $id;
                        }
                        $taxa{$id} = $line;
                        $id_leaves{$node} = $id;
                        $_=~s/$id\:$dist\{$node\}//;
                    }
                    if(/^\\\)\:(\d+\.\d+)\{(\d+)\}/){
                        my ($dist, $node_name) = ($1,$2);
                        my $node = pop @nodes;
                        my $value = $1;				
                        $distances{$node_name} = $value;		
                        $id_nodes{$node_name} = $node;
                        $_=~s/\\\)\:$1\{$2\}//;
                    }	    
                }while(/\)\:(\d+\.\d+)/);
            }
            if (/\)\;/) {
                do{
                    if(/^([^\:]+)\:(\d+\.\d+)\{(\d+)\}/){
                        my ($id,$dist,$node) = ($1,$2,$3);
                        my $value =  $dist;
                        $leaves{$1} = $value;
                        @nodes = sort {$a<=>$b} @nodes;
                        my $line = join ",", @nodes;
                        foreach(@nodes){
                            $node_leaves{$_} .= ",". $id;
                        }
                        $taxa{$id} = $line;
                        $id_leaves{$node} = $id;
                        $_=~ s/$id\:$dist\{$node\}//;
                    }
                    if(/^\\\)\;/){
                        my $node = pop @nodes;
                        $_=~s/\\\)\;//;
                    }	    
                }while(/\)\:(\d+\.\d+)/);
            }
            
        }
        my @keys = sort {$a cmp $b} keys %{$info{ass}};
        if (!@leave_ids) {
            @leave_ids = sort {$a cmp $b} values %id_leaves;
        }
	
	while (@keys) {
		my $key = shift @keys;
		my $metagenome = $1 if $key=~/^(\d+)\_/;
		my ($ass_node,$final_ass,$min_lh,$acc_pos,$total) = (0,0,0,0);
		my $scalar = scalar keys %{$info{ass}{$key}};
		my $sum_weight;
		my %tmp;
		if ($info{ass}{$key}{0}{weight}>0.75) {
			$ass_node = $info{ass}{$key}{0}{node};
			$probabilities{$metagenome}{$ass_node}++;
			$probabilities_adj{$metagenome}{$ass_node}++;
			$good{$metagenome}{$ass_node}++;
			my $lh = $info{ass}{$key}{0}{likelihood};
			$min_lh = $lh;
			$final_ass = 'yes';
			$acc_pos = $info{ass}{$key}{0}{weight};
			$total = 1;
		}else{
			if ($info{ass}{$key}{0}{weight}>=0.35) {
				$good{$metagenome}{$ass_node}++;
			}
			for(my $i = 0; $i < scalar keys %{$info{ass}{$key}}; $i ++){
				my $node = $info{ass}{$key}{$i}{node};
				my $lh = $info{ass}{$key}{$i}{likelihood};
				my $weight = $info{ass}{$key}{$i}{weight};
				if ($min_lh>$lh) {
				    $min_lh = $lh;
				    $ass_node = $node;
				}elsif($min_lh == $lh){
				    $ass_node .= " " . $node;
				}
                                $tmp{$node}{lh} = $info{ass}{$key}{$i}{likelihood};
				$tmp{$node}{w} = $info{ass}{$key}{$i}{weight};
				$sum_weight += $info{ass}{$key}{$i}{weight};
				my $ass = 'yes';
				my $ass2;
				if ($id_leaves{$node}) {
				    my $leave = $id_leaves{$node};
				    my $leave1 = $1 if $leave=~/^(\S{4}\d{3})/;
				    if (grep /^$leave1$/, @core) {
					$ass2 = $leave1;
				    }else{
					$ass = 'not';
				    }
				}elsif($id_nodes{$node}){
				    my $leaves = $node_leaves{$id_nodes{$node}};
				    my @leaves = split ",", $leaves;
				    shift @leaves;
				    while (@leaves) {
					my $leave = shift @leaves;
					my $leave1 = $1 if $leave=~/^(\S{4}\d{3})/;
					if (grep /^$leave1$/, @core) {
					    $ass2 .= "," . $leave1;
					}else{
					    $ass = 'not';
					}
				    }
				}
				$total++;
				if ($ass eq 'yes') {
				    $acc_pos++;
				}
				if ($node eq $ass_node) {
				    $final_ass = $ass;
				}
				
			}
			foreach(keys %tmp){
				my $node = $_;
				$probabilities{$metagenome}{$node} += $tmp{$node}{w};
				my $weight = sprintf "%.3f", ($tmp{$node}{w} / $sum_weight);
				$probabilities_adj{$metagenome}{$node} += $weight;
			}
			if ($ass_node=~/\s/) {
				my @tmp = split /\s/, $ass_node;
				my @tmp_leaves;
				while (@tmp) {
				    my $tmp_node = shift @tmp;
				    if ($id_leaves{$tmp_node}) {
					push @tmp_leaves, $id_leaves{$tmp_node} unless grep /^$id_leaves{$tmp_node}$/, @tmp_leaves;
					
				    }elsif($id_nodes{$tmp_node}){
					my $leaves = $node_leaves{$id_nodes{$tmp_node}};
					my @leaves = split /,/, $leaves;
					shift @leaves;
					while (@leaves) {
					    my $tmptmp = shift @leaves;
					    push @tmp_leaves, shift @leaves, unless grep /^$tmptmp$/, @tmp_leaves;
					}
				    }
				    
				}
				my $tmp_leaves = join ",", sort {$a cmp $b} @tmp_leaves;
				my @keys = keys %id_nodes;
				my %result;
				while (@keys) {
				    my $key = shift @keys;
				    my $node = $id_nodes{$key};
				    my @leaves = sort {$a cmp $b} split /\,/, $node_leaves{$node};
				    shift @leaves;
				    my $leaves = join ",", @leaves;
				    my $pattern = join '&&', @leaves;;
				    if ($tmp_leaves eq $leaves) {
					$ass_node = $key;
					undef @keys;
				    }elsif($leaves=~/$tmp_leaves[0]/){
					my $in = 'yes';
					foreach(@tmp_leaves){
					    if ($leaves!~/$_/) {
						$in = 'no';
					    }
					}
					if ($in eq 'yes') {
					    $result{$key} = scalar @leaves;
					}
					
				    }
				    
				}
				if ($ass_node =~/\s/) {
				    my @keys = sort {$result{$a} <=> $result{$b}} keys %result;
				    $ass_node = $keys[0];
				}
			}
		}
		
		$results{$key}{node} = $ass_node;
		$results{$key}{lh} = $min_lh;
		$results{$key}{profile} = $profile;
		if ($id_leaves{$ass_node}) {
		    $results{$key}{dist} = $leaves{$id_leaves{$ass_node}};
		}elsif($distances{$ass_node}){
		    $results{$key}{dist} = $distances{$ass_node};
		}
		$results{$key}{profile} = $1 if $id=~/\.(\d+)\D/;
		if ($final_ass eq 'yes' ){
			$results{$key}{assignation} = 1;
			$results{$key}{percentage} = $acc_pos / $total;
		}else{
			$results{$key}{assignation} = 0;
			$results{$key}{percentage} = 1 - $acc_pos / $total;
		}
		if ($results{$key}{percentage} >= 0.7) {
			my $leaves;
			
			if($node_leaves{$id_nodes{$ass_node}}){
			    $leaves = $node_leaves{$id_nodes{$ass_node}};
			}elsif($id_leaves{$ass_node}){
			    $leaves = $id_leaves{$ass_node};
			}
			my @leaves = split /\,/, $leaves;
			my $mtg = $1 if $key =~/^(\d+)\_/;
			$freqs2{$mtg}{$ass_node}++;
			while (@leaves) {
			    my $leave = shift @leaves;
			    next if !$leave;
			    $freqs{$mtg}{$leave}++;
			}
		}
	}
}
if ($method ==1 || $method == 3) {
	open(OUT1, ">", $outpath . "EPA_assignations.txt");
	open(OUT6, ">", $outpath . "EPA_node_per_profile.txt");
	print OUT1 "#READ\tprofile\tnode\tLikelihood\tdist\tassignation\tSupport\n";
	print OUT6 "#NODE\tPROFILE\tHIT\n";
	my @keys = sort {$a cmp $b} keys %results;
	my %profiles;
	while (@keys) {
	    my $read = shift @keys;
	    my $lh = $results{$read}{lh};
	    my $ass = $results{$read}{assignation};
	    my $node = $results{$read}{node};
	    my $profile =  $results{$read}{profile};
	    my $dist = sprintf "%.4f", $results{$read}{dist};
	    my $support = sprintf "%.3f", $results{$read}{percentage};
	    print OUT1 $read ."\t" . $profile .  "\t" . $node . "\t" . $lh . "\t" . $dist . "\t" . $ass . "\t" . $support . "\n";
	    $profiles{$profile}{$node}++;
	}
	close OUT1;
	my @profiles = sort {$a <=> $b} keys %profiles;
	while (@profiles) {
		my $profile = shift @profiles;
		for(my $j = 0; $j<=$max_node; $j++){
			print OUT6 $profile . "\t" . $j . "\t" . $profiles{$profile}{$j} . "\n";	
		}
	}
	close OUT6;
	open(OUT2, ">", $outpath . "EPA_leave_frequencies.txt");
	print OUT2 "#METAGENOME";
	for(my $j = 0; $j<scalar @leave_ids;$j++){
	    print OUT2 "\t" . $leave_ids[$j];
	}
	print OUT2 "\n";
	@keys = sort {$a cmp $b} keys %freqs;
	while (@keys) {
	    my $mtg = shift @keys;
	    my $line = $mtg;
	    for(my $j = 0; $j<scalar @leave_ids;$j++){
		my $val;
		my $leave = $leave_ids[$j];
		if (!$freqs{$mtg}{$leave}) {
		    $val = 0;
		}else{
		    $val = $freqs{$mtg}{$leave};
		}
		$line .= "\t" . $val; 
	    }
	    $line .= "\n";
	    print OUT2 $line;
	}
	close OUT2;
	open(OUT3, ">", $outpath . "EPA_node_frequencies.txt");
	@keys = sort {$a cmp $b} keys %freqs2;
	while (@keys) {
	    my $mtg = shift @keys;
	    my $line = $mtg;
	    for(my $j = 0; $j<=$max_node;$j++){
		my $val;
		my $leave = $leave_ids[$j];
		if (!$freqs{$mtg}{$leave}) {
		    $val = 0;
		}else{
		    $val = $freqs{$mtg}{$leave};
		}
		$line .= "\t" . $val; 
	    }
	    $line .= "\n";
	    print OUT3 $line;
	}
}
if ($method == 2 || $method == 3) {
	open(OUT4, ">", $outpath . "EPA_probabilities.txt");
	print OUT4 "#MTG\tNODE\tPROB\n";
	my @keys = sort {$a <=> $b} keys %probabilities;
	while (@keys) {
		my $key = shift @keys;
		for (my $i = 0; $i <=$max_node;$i++){
			print OUT4  $key . "\t" .$i . "\t" . $probabilities{$key}{$i} . "\n"; 	
		}
	}
	
}
open(OUT5,">", $outpath . "EPA_high_weight.txt");
my @keys = sort {$a cmp $b} keys %good;
while (@keys) {
	my $key = shift @keys;
	for(my $i = 0; $i<$max_node; $i++){
		my $val = $good{$key}{$i};
		$val = 0 if !$val;
		print OUT5  $key . "\t" .$i . "\t" . $val . "\n"; 	
	}
}
close OUT5;

exit;


sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}