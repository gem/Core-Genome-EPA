#!usr/bin/perl

use strict;

chomp(my $folder  = $ARGV[0]);
chomp(my $seqfolder = $ARGV[1]);

chomp(my $filter1 = $ARGV[2]);
chomp(my $filter2 = $ARGV[3]);

my $outfolder = $folder;
my @outfolder = split /\//, $outfolder;
pop @outfolder; $outfolder = join "/" , @outfolder; $outfolder .= "/sequences/";mkdir $outfolder unless -d $outfolder;

my $getentry = '/usr/local/bin/getentryF';
opendir(SEQFOLDER,$seqfolder);
my @subs = grep !/\./, readdir SEQFOLDER;
closedir SEQFOLDER;
my %subs;
while (@subs) {
    my $subid = shift @subs;
    my $sub = $seqfolder . $subid . "/";
    opendir SUB, $sub;
    my @files = grep /\.aa90\./, readdir SUB;
    closedir SUB;
    while (@files) {
        my $id = shift @files;
        $id = $1 if $id=~/^(\d{7})/;
        $subs{$id} = $subid;
    }
    
}



opendir FOLDER, $folder;
my @files  = grep /.out/,readdir FOLDER;
closedir FOLDER;

while(@files){
    my $id = shift @files;
    my %include;
    my $file = $folder . $id;
    $id = $1 if $id=~/^(\d{7})/;
    my $subdir = $subs{$id};
    my $sub = $seqfolder . $subs{$id} . "/" . $id . ".cluster.aa90.faa";
    open(FILE, $file);
    my $outlist = $outfolder . $id . ".lst";
    open(OUT, ">", $outlist);
    while (<FILE>) {
        chomp;
        my @line = split /\t/, $_;
        next if $line[1] == 0 || $line[2] == 0;
        next if $line[1] < $filter1;
        my $div = $line[1]/$line[2];
        next if $div <= $filter2;
        next if $line[3]>$line[1] && $line[3] > $line[2];
        print OUT $line[0] . "\n";
    }
    close OUT;
    my $outseq = $outfolder . $id . ".selected.faa";
    my $query = $getentry . " -f $outlist $sub > $outseq";
    qx/$query/;
    
}