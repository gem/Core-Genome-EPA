#!usr/bin/perl

use strict;
use Time::Local;
my %opts;
&getopt(\%opts);

if(exists($opts{'h'})||(scalar(keys(%opts))==0)) 
    #HELP OUTPUT
{ 
	print "usage: $0 [options]\n";
	print " -h this message\n";
	print " -s Sequence Folder (REQUIRED)\n";
        print " -r Results Folder (REQUIRED)\n";
        print " -o Output Folder (optional)\n";
        print " -m Minimum score (REQUIRED)\n";
        print " -d/D Minimum/Maximum ratio score (REQUIRED)\n";
    exit;
} 
my $time1 = timegm(gmtime());

my $seqs = $opts{'s'};
if($seqs !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $seqs = $pwd . "/" . $seqs;
}
die "This directory does not exist. Aborting\n" unless -d $seqs;

my $results = $opts{'r'};
if($results !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $results = $pwd . "/" . $results;
}
die "This directory does not exist. Aborting\n" unless -d $results;

my $output = $opts{'o'};
if (! $output) {
    $output = $results;
}

if($output !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $output = $pwd . "/" . $output;
}
mkdir $output unless -d $output;

my($min,$max,$score);
if ($opts{'D'}) {
    $max = $opts{'D'};
}elsif($opts{'d'}){
    $min = $opts{'d'};
}else{
    die "Ratio threshold not defined. Aborting\n";
}
$score = $opts{'m'};
die "minimum threshold not defined. Aborting\n" unless $opts{'m'};


opendir RESULTS, $results;
my @files = grep /\.txt/, readdir RESULTS;
closedir RESULTS;

while (@files) {
    my $id = shift @files;
    my $result_file = $results . $id;
    $id =~s/HMM_scores_together.txt//;
    my $seq_file = $seqs . $id . ".sim";
    next "Warning: File $seq_file appears not to be where it should. Keep it in mind\n" if ! -e $seq_file;
    open(RES, $result_file);
    my $selected = '||';
    my %data;
    while (<RES>) {
            chomp;
        my @line = split /\t/, $_;
        my $name = $line[0];
        my $profile = $line[2];
        $profile = $1 if $profile=~/\.(\d+)\./;
        
        next if $line[3] == 0 || $line[4] == 0;
        my $tmp1 = $line[3]/$line[4];
        if ($max) {
            next if $line[4]<$score;
            next if $tmp1 > $max;
            $selected .=  $name . "||";
            $data{$name} = $profile;
        }elsif($min){
            next if $line[3]<$score;
            next if $tmp1 <= $max;
            $selected .=  $name . "||";
            $data{$name} = $profile;
        }
        
    }
    close RES;
    open(SEQ, $seq_file);
    my $name;
    while (<SEQ>) {
        chomp;
        if (/>(\S+)/) {
            close OUT;
            undef $name;
            my $tmp = $1;
            if ($selected =~/\|\|$tmp\|\|/) {
                my $profile = $data{$tmp};
                my $out = $output . $id . '.selected.' . $profile . '.fna';
                open(OUT, ">>", $out);
                print OUT ">" . $tmp . "\n";
                $name = $tmp;
            }
            
        }elsif($name){
            print OUT $_ . "\n";
        }
        
    }
}
my $time2 = timegm(gmtime());
my $dif = $time2 - $time1;
print "\nProcess Finished in " . $dif . " seconds!\n";
exit;

sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}


sub empty_value{    
    our($args,%opts)=@_;
    my ($return,$val,@keys);
    if($args eq 'bi'){
        $keys[0]='bi';
    }else{
        @keys=split '',$args;
    }
    foreach (@keys) {
        if(!defined($opts{$_})){
            $val=$_;
            if ($val eq 'p'){
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    system('stty','-echo');
                    chop(my $value=<STDIN>); #hide the value for password
                    system('stty','echo');
                    
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                    }
            
                }    
            }else{
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    my $value=<STDIN>;
                    chomp($value);
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                        }
            
                }
            }
            if(!defined($opts{$_})){$return=$val;return $return;}
        } 
    }
    $return=0;
    return ($return);
}