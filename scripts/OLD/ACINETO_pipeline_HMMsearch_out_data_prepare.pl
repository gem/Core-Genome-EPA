#!usr/bin/perl

use strict;
use Time::Local;
my %opts;
&getopt(\%opts);


if(exists($opts{'h'})||(scalar(keys(%opts))==0)) 
    #HELP OUTPUT
{ 
	print "usage: $0 [options]\n";
	print " -h this message\n";
	print " -u first database (REQUIRED)\n";
        print " -d second database (REQUIRED)\n";
        print " -t third database (REQUIRED)\n";
        print " -p Path (REQUIRED)\n";
	print " -l length file (OPTIONAL in full path)\n";
	print " -o outpath (OPTIONAL)\n";
    exit;
} 
my $time1 = timegm(gmtime());

my $path = $opts{'p'};
if($path !~/^\//){
    chomp(my $pwd = qx/pwd/);
    $path = $pwd . "/" . $path;
}
die "This directory does not exist. Aborting\n" unless -d $path;

my $first = $opts{'u'};
my $second = $opts{'d'};
my $third = $opts{'t'};

my %data;
my $length;

my $length_line;
my %lengths;
if (!$opts{l}) {
	$length = 'NULL';
}elsif(-d $opts{l}){
	$length = 'HASH';
}else{
	$length = 'HASH';
	die "Length file does not exist. Aborting\n" unless -e $opts{l};
	print "Reading the length file....";
	open(LEN, $opts{l});
	my $i = 0;
	while (<LEN>) {
		chomp;
		if ($i == 10000) {
			print ".";
			$i = 0;
		}
		$i++;
		$lengths{$1} = $2 if /^([^\t]+)\t(\d+)/;
	}
	close LEN;
	print "Done!\n";
}


opendir PATH, $path;
my @first = grep /$first/, readdir PATH;
closedir PATH;

my @ln;
if (-d $opts{l}) {
	@ln = @first;
}
print "Analyzing first database in systems...";
while (@first) {
	my $id = shift @first;
	my $file = $path . $id;
	my ($sim_name) = $1 if $id=~/^(\S+).$first/;
	$sim_name=$1 if $sim_name=~/^(\d{7})/;
	print "$sim_name";
	open(FILE, $file);
	my $i = 0;
	while (<FILE>) {
		chomp;
		next if /^\#/;
		if ($i == 5000) {
			$i = 0;
			print ".";
		}
		~s/\s+/\t/g;
		my @line = split /\t/, $_;
		my ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
		next if $ev > 0.05;
		my $len;
		if ($length eq 'HASH') {
			
		}else{
			$len = $1 if $name=~/\_(\d+)\_[\+\-]$/;
		}
		if(!$data{$sim_name}{$name}){
			$data{$sim_name}{$name}{$first}{profile} = $profile;
			$data{$sim_name}{$name}{$first}{score} = $score;
			$data{$sim_name}{$name}{$first}{ev} = $ev;
			$i++;
		}elsif($data{$sim_name} && $data{$sim_name}{$name}{$first}{ev}> $ev){
			$data{$sim_name}{$name}{$first}{profile} = $profile;
			$data{$sim_name}{$name}{$first}{score} = $score;
			$data{$sim_name}{$name}{$first}{ev} = $ev;
			$i++;
		}
	}
	close FILE;
	print "\n";
}
print "Done!\n";
if (-d $opts{l}) {
	print "Reading the length files....";
	while (@ln) {
		my $sim_name = shift @ln;
		$sim_name = $1 if $sim_name=~/^(\d{7})/;
		print $sim_name;
		my $lenfile = $opts{l} . $sim_name . ".lnt";
		open(LEN, $lenfile);
		my $i = 0;
		while (<LEN>) {
		chomp;
		if ($i == 1000) {
			print ".";
			$i = 0;
		}
		$i++;
		my ($name,$len) = ($1, $2) if /^([^\t]+)\t(\d+)/;
		next if ($data{$sim_name} && !$data{$sim_name}{$name});
		$data{$sim_name}{$name}{$first}{score} /= $len;
		$name = $sim_name . "_" . $name;
		$lengths{$name} = $len;
	}
	close LEN;
	print "\n";
	}
	print "Done!\n";
}
print "Starting the second database in systems...";

opendir PATH, $path;
my @second = grep /$second/, readdir PATH;
closedir PATH;
while (@second) {
	my $id = shift @second;
	my $file = $path . $id;
	my ($sim_name) = $1 if $id=~/^(\S+).$second/;
	print "$sim_name";
	my $i = 0;
	open(FILE, $file);
	while (<FILE>) {
		chomp;
		next if /^\#/;
		~s/\s+/\t/g;
		my @line = split /\t/, $_;
		my ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
		if ($i == 5000) {
			$i = 0;
			print ".";
		}
		
		next if $ev > 0.05;
		my $len;
		if ($length eq 'HASH') {
			my $id = $sim_name . "_" . $name;
			$len = $lengths{$id};
		}else{
			$len = $1 if $name=~/\_(\d+)$/;
		}
		next if ($data{$sim_name} && !$data{$sim_name}{$name});
		if (!$len) {
			$len=1;
			print "WARNING: No Length for " . $name . "\n";
		}
		if(!$data{$sim_name}{$name}{$second}){
			
			$data{$sim_name}{$name}{$second}{score} = $score/$len;
			$data{$sim_name}{$name}{$second}{ev} = $ev;
			$i++;
		}elsif($data{$sim_name} && $data{$sim_name}{$name}{$second}{ev}> $ev){
			
			$data{$sim_name}{$name}{$second}{score} = $score/$len;
			$data{$sim_name}{$name}{$second}{ev} = $ev;
			$i++;
		}
	}
	close FILE;
}
print "Done!\n";
if ($third) {
	print "Last but not least, the third database in systems...";
	opendir PATH, $path;
	my @third = grep /$third/, readdir PATH;
	closedir PATH;
	while (@third) {
		my $id = shift @third;
		my $file = $path . $id;
		my ($sim_name) = $1 if $id=~/^(\S+).$third/;
		print "$sim_name";
		my $i = 0;
		open(FILE, $file);
		while (<FILE>) {
			chomp;
			next if /^\#/;
			~s/\s+/\t/g;
			my @line = split /\t/, $_;
			my ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
			next if $ev > 0.05;
			my $len;
			if ($i == 5000) {
				print ".";
				$i = 0;
			}
			
			if ($length eq 'HASH') {
				my $id = $sim_name . "_" . $name;
				$len = $lengths{$id};
			}else{
				$len = $1 if $name=~/\_(\d+)$/;
			}
			next if ($data{$sim_name} && !$data{$sim_name}{$name});
			if (!$len) {
				$len=1;
				print "WARNING: No Length for " . $name . "\n";
			}
			if(!$data{$sim_name}{$name}{$third}){
				$data{$sim_name}{$name}{$third}{score} = $score/$len;
				$data{$sim_name}{$name}{$third}{ev} = $ev;
				$i++;
			}elsif($data{$sim_name} && $data{$sim_name}{$name}{$third}{ev}> $ev){
				$data{$sim_name}{$name}{$third}{score} = $score/$len;
				$data{$sim_name}{$name}{$third}{ev} = $ev;
				$i++;
			}
		}
		close FILE;
	}	
}


print "Done! Now let's wrap everything up!";

my @systems = sort {$a cmp $b} keys %data;
my $outpath;
if ($opts{o}) {
}else{
	$outpath = $path;
}

while (@systems) {
	my $sys = shift @systems;
	open(OUT, ">", $outpath . $sys . "_HMM_scores_together.txt");
	my @frags = sort {$a cmp $b} keys %{$data{$sys}};
	while (@frags) {
		my $frag = shift @frags;
		my ($one,$two,$three,$profile);
		if ($data{$sys}{$frag}{$first}) {
			$one =  $data{$sys}{$frag}{$first}{score};
			$profile =  $data{$sys}{$frag}{$first}{profile};
		}else{
			$profile = 'NA';
			$one = 0;
		}
		if ($data{$sys}{$frag}{$second}) {
			$two =  $data{$sys}{$frag}{$second}{score};
			
		}else{
			$two = 0;
		}
		if ($data{$sys}{$frag}{$third}) {
			$three =  $data{$sys}{$frag}{$third}{score};
		}else{
			$three = 0;
		}
		next if ($one == 0 || $two == 0);
		print OUT $frag . "\t" . $sys . "\t" . $profile .  "\t" . $one . "\t" . $two . "\t" . $three . "\n";
	}
	close OUT;
	
	
}
my $time2 = timegm(gmtime());
my $dif = $time2 - $time1;

print "\nProcess Finished in " . $dif . " seconds!\n";


exit;

sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}


sub empty_value{    
    our($args,%opts)=@_;
    my ($return,$val,@keys);
    if($args eq 'bi'){
        $keys[0]='bi';
    }else{
        @keys=split '',$args;
    }
    foreach (@keys) {
        if(!defined($opts{$_})){
            $val=$_;
            if ($val eq 'p'){
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    system('stty','-echo');
                    chop(my $value=<STDIN>); #hide the value for password
                    system('stty','echo');
                    
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                    }
            
                }    
            }else{
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    my $value=<STDIN>;
                    chomp($value);
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                        }
            
                }
            }
            if(!defined($opts{$_})){$return=$val;return $return;}
        } 
    }
    $return=0;
    return ($return);
}