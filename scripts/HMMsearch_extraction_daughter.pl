#!usr/bin/perl

#use strict;
print $0 . "\n";
my @args = @ARGV;
use File::chdir;
#my $q = $script . " " . $input1 . " " . $input2  . " " . $input3 . " " . $output . " " . $output2 . " " . $reads . " " . $len .  " " . $score1 . " " . $score2 .  " &";
my $getentry = '/usr/local/bin/getentryX';
my ($db1,$db2,$db3,$out,$out2,$reads,$score1,$score2) = ($ARGV[0],$ARGV[1],$ARGV[2],$ARGV[3],$ARGV[4],$ARGV[5],$ARGV[6],$ARGV[7]);
my ($id) = ($1) if $reads=~/(\d{7})/;
my $outpath = $1 if $out=~/^(.*\/)$id/;
mkdir $outpath . $id . "/" unless -d $outpath . $id;
my $outfolder = $outpath . $id . "/";
my %headers;
open(DB1, $db1);
my $i = 0;
my %data;
my $type;
my %lengths;
while (<DB1>) {
        chomp;
        
        if (/^#/&&/target\sname/ && /from\s+to\s+from\s+to\s+from\s+to\s+/) {
                $type = 'dom';
        }elsif(/^#/&&/target\sname/){
                $type = 'tab';  
        }else{
                
        }
        
        next if /^\#/;
        ~s/\s+/\t/g;
        my @line = split /\t/, $_;
        my ($name,$profile,$ev,$score,$len1,$len2);
        if ($type eq 'dom') {
               ($name,$len1,$profile,$len2,$ev,$score) = ($line[0],$line[2],$line[3],$line[5],$line[6],$line[7]);
        }else{
               ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
        }
        my $header_tmp = $name;
        $profile=$1 if $profile=~/\.(\d+)\./;
        $name =~s/\#/\_/g;
        $name =~s/\:/\_/g;
        $name =~s/\//\_/g;
        next if $ev > 0.05;
        my $len;
        if(!$data{$name}){
                $data{$name}{one}{profile} = $profile;
                $data{$name}{one}{score} = $score;
                $data{$name}{one}{ev} = $ev;
                $lengths{$name} = $len1;
                $i++;
        }elsif($data{$name} && $data{$name}{one}{ev}> $ev){
                $data{$name}{one}{profile} = $profile;
                $data{$name}{one}{score} = $score;
                $data{$name}{one}{ev} = $ev;
                $i++;
        }
        $headers{$name} = $header_tmp;
}
close DB1;
open(DB2, $db2);
while (<DB2>) {
        chomp;
        if (/^#/&&/target\sname/ && /from\s+to\s+from\s+to\s+from\s+to\s+/) {
                $type = 'dom';
        }elsif(/^#/&&/target\sname/){
                $type = 'tab';  
        }else{
                
        }
        next if /^\#/;
        ~s/\s+/\t/g;
        my @line = split /\t/, $_;
        my ($name,$profile,$ev,$score,$len1);
        if ($type eq 'dom') {
               ($name,$len1,$profile,$ev,$score) = ($line[0],$line[2],$line[3],$line[6],$line[7]);
        }else{
               ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
        }
        $name =~s/\#/\_/g;
        $name =~s/\:/\_/g;
        $name =~s/\//\_/g;
        next if $ev > 0.05;
        my $len;
        next if (%data && !$data{$name});
        if($data{$name} && !$data{$name}{two}){
                $data{$name}{two}{profile} = $profile;
                $data{$name}{two}{score} = $score;
                $data{$name}{two}{ev} = $ev;
                $lengths{$name} = $len1 unless $lengths{$name};
        }elsif($data{$name} && $data{$name}{two}{ev}> $ev){
                $data{$name}{two}{profile} = $profile;
                $data{$name}{two}{score} = $score;
                $data{$name}{two}{ev} = $ev;
        }
}
close DB2;


open(DB3, $db3);
while (<DB3>) {
        chomp;
        if (/^#/&&/target\sname/ && /from\s+to\s+from\s+to\s+from\s+to\s+/) {
                $type = 'dom';
        }elsif(/^#/&&/target\sname/){
                $type = 'tab';  
        }else{
                
        }
        next if /^\#/;
        ~s/\s+/\t/g;
        my @line = split /\t/, $_;
        my ($name,$profile,$ev,$score,$len1);
        if ($type eq 'dom') {
               ($name,$len1,$profile,$ev,$score) = ($line[0],$line[2],$line[5],$line[6],$line[7]);
        }else{
               ($name,$profile,$ev,$score) = ($line[0],$line[2],$line[4],$line[5]);
        }
        $name =~s/\#/\_/g;
        $name =~s/\:/\_/g;
        $name =~s/\//\_/g;
        next if $ev > 0.05;
        my $len;
        next if (%data && !$data{$name});
        if($data{$name} && !$data{$name}{three}){
                $data{$name}{three}{profile} = $profile;
                $data{$name}{three}{score} = $score;
                $data{$name}{three}{ev} = $ev;
                $lengths{$name} = $len1 unless $lengths{$name};
                $i++;
        }elsif($data{$name} && $data{$name}{three}{ev}> $ev){
                $data{$name}{three}{profile} = $profile;
                $data{$name}{three}{score} = $score;
                $data{$name}{three}{ev} = $ev;
                $i++;
        }
}
close DB3;


$outpath .= "/TMP/";
mkdir $outpath unless -d $outpath;
open(TAB, ">",  $out);
my @keys = sort {$a cmp $b} keys %data;
open(OUT, ">", $out2);
$CWD = "$outpath";
while (@keys) {
    my $name = shift @keys;
    my $len = $lengths{$name};
    my $profile = $data{$name}{one}{profile};
    $profile = $1 if $profile=~/\.(\d{1,3})\./;
    my $db1 = $data{$name}{one}{score};
    my $db2 = $data{$name}{two}{score};
    my $db3 = $data{$name}{three}{score};
    if (!$len) {
    }else{
        $db1 /= $len;
        if (!$db2) {
            $db2 = 0;
        }
        $db2 /= $len;
        if (!$db3) {
            $db3 = 0;
        }
        $db3 /= $len;
        print TAB $name . "\t" . $profile . "\t" . $db1 . "\t" . $db2 . "\t" . $db3 . "\n";
        next if $db1<1.5;
        next if $db2 == 0;
        next if $db3 > $db1;
        next if ($db1/$db2) < 1;
        print OUT $headers{$name} . " " . $id . "_" . $profile . ".fa". "\n";
    }
}
close TAB;
close OUT;
qx/$getentry -f $out2 $reads/ unless !$out2;
qx/mv $id*.fa $outfolder/ unless !$out2;
undef %headers;
undef %data;
undef $lengths;

exit;
