package Functions;

use strict;
use File::Temp qw(tempdir);
use Time::Local;
use Scalar::Util qw(looks_like_number);
my $query = 'ps -ef | grep -i _daughter.pl';
my $script = $0;
my @script_pipe = split /\//, $script;
pop @script_pipe;
my $script_pipe = join "/", @script_pipe;
$script = "$script_pipe/scripts/HMMsearch_extraction_daughter.pl";
my $matrix = "$script_pipe/scripts/blosum62.txt";

sub mother{
	#get vars
	my ($db1,$db2,$db3,$inpath,$reads,$outpath,$score1,$score2,$num) = @_;
	my %files;
	my $eval  = looks_like_number($score1);
	#errors
	return(0) if $eval == 0;
	return(0) if $score1 < 1;
	return(1) if $score2 == 0;
	return(1) if $score2 <1;
	
	#rename vars
	my $folder = $inpath;
	my $reads_folder = $reads;
	my $first  = $db1;
	my $second = $db2;
	my $third  = $db3;
	#create tmp folder
	my $tmpdir = $outpath . '/TMP/';
	mkdir $tmpdir unless -d $tmpdir;
	return(2) if !-d $outpath;
	my $length = 'NULL';
	#get HMMsearch output files
	opendir(FOLDER, $folder);
	my %dir2file;
	opendir READS, $reads_folder;
	my @reads = grep !/^\./, readdir READS;
	my @dirs = @reads;
	closedir READS;
	my $scalar = scalar @reads;
	return(3) if $scalar == 0;
	while (@dirs) {
		my $dir_tmp = shift @dirs;
		my $file_tmp = $1 if $dir_tmp =~/^([^\.]+)/;
		my $dir = $reads_folder . $dir_tmp;
		$dir2file{$file_tmp} = $dir;
	}
	my @files = grep /$first/&&/\.out/, readdir FOLDER;
	my $scalar2 = scalar @reads;
	return(4) if $scalar == 0;
	return(5) if $scalar2 != $scalar;
	while (@files) {
		my $file_id = shift @files;
		my $input= $folder . $file_id;
		$file_id = $1 if $file_id=~/^(\d{7})/;
		next if -e $tmpdir . "/" . $file_id . '.hmmout';
		$files{$file_id}{input}{db1} = $input;
		$input=~s/$first/$second/;
		$files{$file_id}{input}{db2} = $input;
		$input=~s/$second/$third/;
		$files{$file_id}{input}{db3} = $input;
		$files{$file_id}{reads} = $dir2file{$file_id};
		$files{$file_id}{out}   = $outpath . "/" . $file_id . '.hmmout';
		$files{$file_id}{out2}  = $outpath . "/" . $file_id . '.lst';
	}
	my @keys = sort {$a cmp $b} keys %files;
	my $max = $num;
	while (@keys) {
		my @out = qx/$query/;
		my $i = 0;
		while (@out) {
			my $tmp = shift @out;
			if ($i == 0) {
				my $val = shift @keys;
				next if !$val;
				print "$val\n";
				my $input1 = $files{$val}{input}{db1};
				my $input2 = $files{$val}{input}{db2};
				my $input3 = $files{$val}{input}{db3};
				my $output1 = $files{$val}{out};
				my $output2 = $files{$val}{out2};
				my $reads = $files{$val}{reads};
				my $q = 'perl ' . $script . " " . $input1 . " " . $input2  . " " . $input3 . " " . $output1 . " " . $output2. " " . $reads  . " " . $score1 . " " . $score2 .  " &";
				system $q unless $output1;
			}
			
			next if $tmp=~/grep/;
			$i++;
			if ($i<=$max) {
				my $val = shift @keys;
				next if !$val;
				print "$val\n";
				my $input1 = $files{$val}{input}{db1};
				my $input2 = $files{$val}{input}{db2};
				my $input3 = $files{$val}{input}{db3};
				my $output1 = $files{$val}{out};
				my $output2 = $files{$val}{out2};
				my $len =  $files{$val}{len};
				my $reads = $files{$val}{reads};
				my $q = 'perl ' . $script . " " . $input1 . " " . $input2  . " " . $input3 . " " . $output1 . " " . $output2. " " . $reads . " " . " " . $score1 . " " . $score2 .  " &";
				system $q unless $output1;
			}else{
			   sleep(1); 
			}
		}
	
	}
	return(-1);
}

sub error{
	my ($error_n) = @_;
	print " Profile search Finished!\n" if $error_n == -1;
	die "DB1 score has to be numeric, and equal or higher than 1. Aborting" if $error_n == 0;
	die "Ratio score has to be numeric, and equal or higher than 1. Aborting" if $error_n == 1;
	die "unable to create or access the Output directory. Aborting\n" if $error_n == 2;
	die "Reads folder contains 0 files. And it shouldn't they were maybe erased?. Aborting\n" if $error_n == 3;
	die "Input folder contains 0 files. And it shouldn't they were maybe erased?. Aborting\n" if $error_n == 4;
	die "Input folder should contain the same amount of files than Read folder. Aborting\n" if $error_n == 5;
	die "Alignment folder Empty, without gene_identifiers, or bad format. Aborting\n" if $error_n == 6;
	die "Mafft Alignment missing. Aborting\n" if $error_n == 7;
	return if $error_n == -2;
}
sub nfile_checker{
	my($in,$out) = @_;
	my @in = split /\//, $in;
	pop @in;
	my $in_short = join "/", @in;
	#check if -d $in
	if (-d $in) {
		opendir IN, $in;
	}elsif(-d $in_short){
		opendir IN, $in_short;
	}
	undef @in;
	opendir OUT,$out;
	@in = grep !/^\./ && /\./, readdir IN;
	my @out = grep !/^\./ && /\./, readdir OUT;
	closedir IN;
	closedir OUT;
	return(scalar(@in),scalar(@out));
}    

sub RmIfEmpty{
	my ($file) = @_;
	return(0) if !-e $file;
	my $size = -s $file;
	qx/rm $file/ if $size == 0;
	return(1);
}

sub OriAlnRenamer{
	my ($path) = @_;
	opendir DIR, $path;
	my @files = grep /\.aln/, readdir DIR;
	if ($files[0]=~/^(\d+)\.original\.aln/) {
		return(-2);
	}
	
	closedir DIR;
	return(0) if scalar @files == 0;
	while (@files) {
		my $id = shift @files;
		my $file_in = $path . $id;
		if ($id=~/(\d+)/) {
			my $out_id = $1 . ".original.aln";
			my $outfile = $path . $out_id;
			qx/mv $file_in $outfile/;
		}
		
	}
	return(-2);
}

sub getopt (;$$) {
    my ($hash) = @_;
    my ($first,$rest);
    local $_;
    my @EXPORT;

    while (@ARGV && ($_ = $ARGV[0]) =~ /^-(.*)/) {
	($first) = ($1);
         $rest='';
	if (/^--$/) {	# early exit if --
	    shift @ARGV;
	    last;
	}
	if ($first ne '') {
	    if ($rest ne '') {
		shift(@ARGV);
	    }
	    else {
		shift(@ARGV);
		$rest = shift(@ARGV);
	    }
	    if (ref $hash) {
	        $$hash{$first} = $rest;
	    }
	    else {
	        ${"opt_$first"} = $rest;
	        push( @EXPORT, "\$opt_$first" );
	    }
	}
	else {
	    if (ref $hash) {
	        $$hash{$first} = 1;
	    }
	    else {
	        ${"opt_$first"} = 1;
	        push( @EXPORT, "\$opt_$first" );
	    }
	    if ($rest ne '') {
		$ARGV[0] = "-$rest";
	    }
	    else {
		shift(@ARGV);
	    }
	}
    }
    unless (ref $hash) { 
	local $Exporter::ExportLevel = 1;
	import Getopt::Std;
    }
}


sub empty_value{    
    our($args,%opts)=@_;
    my ($return,$val,@keys);
    if($args eq 'bi'){
        $keys[0]='bi';
    }else{
        @keys=split '',$args;
    }
    foreach (@keys) {
        if(!defined($opts{$_})){
            $val=$_;
            if ($val eq 'p'){
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    system('stty','-echo');
                    chop(my $value=<STDIN>); #hide the value for password
                    system('stty','echo');
                    
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                    }
            
                }    
            }else{
                my $i = 0;
                until ($i>=2){
                    
                    print "please introduce a value for $_ :  ";   
                    
                    my $value=<STDIN>;
                    chomp($value);
                    if($value eq ''){$i++;}else{
                        $opts{$_}=$value;
                        $i=2;
                        }
            
                }
            }
            if(!defined($opts{$_})){$return=$val;return $return;}
        } 
    }
    $return=0;
    return ($return);
}

sub joiner{
	my ($dir) = @_;
	my $log = $dir . 'joined.log';
	open(LOG, $log);
	chomp(my @log = <LOG>);
	close LOG;
	open(LOG, ">>", $log);
	opendir DIR, $dir;
	my @info = grep !/^\./, readdir DIR;
	closedir DIR;
	mkdir "$dir/joined/" unless -d "$dir/joined/";
	while (@info) {
		my $tmp = shift @info;
		next if $tmp eq 'joined';
		my $path = $dir . $tmp;
		if (-d $path) {
			opendir PATH, $path;
			next if grep /^$tmp$/, @log;
			print LOG $tmp . "\n";
			my @files = grep /\.fa/, readdir PATH;
			closedir PATH;
			while (@files) {
				my $id = shift @files;
				my $file = $path ."/". $id;
				$id= $1 if $id=~/$tmp\_(.*).fa/;
				open(FILE, $file);
				open(OUT,">>", "$dir/joined/$id.fa");
				while (<FILE>) {
					if (/>/) {
						~s/>/>$tmp\_/;
					}else{
					}
					print OUT $_;
				}
				
			}
			
		}
		
	}
	close LOG;
	
}
sub RefGapTrimmer{
	my ($file,$out,$ref,$accepted) = @_;
	return(7) unless -e $file;
	$accepted=~s/\s+/ /g;
	$accepted=$1 if $accepted=~/^\s(.*)\s$/;
	my %seqs = &parser_fasta($file);
	my @keys = keys %seqs;
	open(OUT, ">", $out);
	my @list = split " ", $accepted;
	my @ref = split " ", $ref; # I keep the list just in case
	my $ln;
	my @out;
	while(@list){
		#foreach element on List
		my $ref = shift @list;
		#I select the sequence associated to the element on list 
		my @seqid = grep /^$ref/, @keys;
		my $seqid = $seqid[0];
		my $seq = $seqs{$seqid};
		#I split the sequence in monomers
		my @seq = split "", $seq;
		$ln = scalar @seq;
		my $i = 0;
		#I create an array (@out), in which if there isn't a gap in that sequence, that position does not change (1).
		#But if it is a gap, the position will be considered as empty and will be removed
		while (@seq) {
			my $pos = shift @seq;
			if ($pos eq '-') {
			$out[$i] = 0 unless $out[$i]==1;
			}else{
			$out[$i] = 1;
			}
			$i++;
		}
	}
	#now I start with ALL the sequences and I remove the positions that are all gaps.
	@keys = sort {$b cmp $a } @keys;
	while (@keys) {
		my $key = shift @keys;
		my $seq = $seqs{$key};
		if ($key=~/\:/) {
			$key=~s/\:/\_/g;
		}else{
			my $totoro = &multiregex($key,@ref);
			do { $key = $1 if $key=~/^(\S{4}\d{3}\S\d{2}\S)/ } if $totoro ==1;
			
		}
		print OUT ">" . $key . "\n";
		my @seq = split "", $seq;
		for(my $i = 0; $i<$ln;$i++){
			if ($out[$i] == 0) {
			#code
			}else{
			print OUT $seq[$i];
			}
		}
		print OUT "\n";
	}
	close OUT;
}
sub DistSeqTrimmer{
	my ($in,$out,$ref,$accepted,$tree) = @_;
	my @tips = get_leaf_nodes($tree);
	my $tips = join " ", @tips;
	my ($num_seq) = 0;
	my ($one,$two,$three,$four);
	my %matrix = matrix_extractor($matrix);
	my %seqs = parser_fasta($in);
	my $max = 0;
	my %dist;
	my @ref1 = split /\s/, $ref;
	my @ref2 = @ref1;
	my %ref;
	my @keys = sort {$a cmp $b} keys %seqs;
	my @queries;
	while (@keys) {
		my $key = shift @keys;
		if ($ref=~/$key/){
			$ref{$key} = $seqs{$key} ;			
		}else{
			push @queries, $key unless !$key;
		}

	}
	open(OUT,">", $out);
	while (@ref1) {
		my $key = shift @ref1;
		my $seq = $seqs{$key};
		do{
			print OUT ">" . $key . "\n" . $seq . "\n"; 
		}if($tips=~/$key/);
	}
	if (!$accepted) {
		$accepted = $ref;
	}elsif ($accepted ne 'NULL') {
		
	}else{
		$accepted = $ref;
	}
	$accepted=~s/\s+/ /g;
	$accepted=$1 if $accepted=~/^\s(.*)\s$/;
	my @accepted = split /\s/, $accepted;
	my @accepted2 = @accepted;
	while (@accepted) {
            my $key1 = shift @accepted;
            my $length = length $seqs{$key1};
            if (!$length) {
                   next;
            }
            my @ks = @accepted;
            while (@ks) {
                my $key2 = shift @ks;
                $dist{$key1}{$key2} = 0 if $key1 eq $key2;
                next if $key1 eq $key2;
                $dist{$key1}{$key2} = $dist{$key2}{$key1} if $dist{$key2}{$key1} >0;
                next if $dist{$key1} && $dist{$key1}{$key2} && $dist{$key2}{$key1} == $dist{$key1}{$key2};
                my @aa1 = split "", $ref{$key1};
                my @aa2 = split "", $ref{$key2};
                my $gapp = 3;
                while (@aa1) {
                    my $aa1 = shift @aa1;
                    my $aa2 = shift @aa2;
                    next if $aa1 eq $aa2;
                    my $penalty;
                    if ($aa1 ne '-' && $aa2 ne '-') {
                        $penalty = abs($matrix{$aa1}{$aa2});
                        $gapp = 3;
                    }else{
                        $penalty = $gapp;
                        $gapp++;
                    }
                    $dist{$key1}{$key2} += $penalty;
                }
                
                
                $dist{$key1}{$key2} /= $length;
                if ($max < $dist{$key1}{$key2}) {
                    $max = $dist{$key1}{$key2};
                    $one = $key1;
                    $two = $key2;
                }
                $dist{$key2}{$key1} = $dist{$key1}{$key2};
            }
        }
        my @new_keys;
        @new_keys = sort {$dist{$one}{$b} <=> $dist{$one}{$a}} keys %{$dist{$one}};
        if (scalar keys %ref>=3) {
            do{
                $three = shift @new_keys;
            }until($three ne $two);
            @new_keys = sort {$dist{$three}{$b} <=> $dist{$three}{$a}} keys %{$dist{$three}};
            if (scalar keys %ref>=4) {
                do{
                    $four = shift @new_keys;
                }until($four ne $three && $four ne $two && $four ne $one);
            }
            
            
        }
        undef %dist;  

	while (@queries) {
	        my $key = shift @queries;
	        my $seq = $seqs{$key};
		my ($left,$sseq,$right);
		if ($seq=~/^[^\-]/&&$seq=~/[^\-]$/) {
		    ($sseq) = ($seq);
		}elsif($seq=~/^[^\-]/){
		    ($left,$sseq,$right) = (0,$1,length($2)) if $seq=~/^(\S+[^-])([-]+)$/;
		}elsif($seq=~/[^\-]$/){
		    ($left,$sseq) = (length($1),$2) if $seq=~/^([-]+)([^\-]\S+)$/;
		}else{
		   ($left,$sseq,$right) = (length($1),$2,length($3)) if $seq=~/^([-]+)([^\-].*[^\-])([-]+)$/;
		}
		my $length = length($sseq);
		my @sseq;
		my @ref;
		push @sseq, substr($seqs{$one},$left,$length);
		push @ref,  substr($seqs{$one},$left,$length);
		push @sseq, substr($seqs{$two},$left,$length);
		push @ref,  substr($seqs{$two},$left,$length);
		push @sseq, substr($seqs{$three},$left,$length);
		push @ref,  substr($seqs{$three},$left,$length);
		push @sseq, substr($seqs{$four},$left,$length);
		push @ref,  substr($seqs{$four},$left,$length);
		my $min_dist = 0;
		my @sseq_tmp = @sseq;
		while (@sseq) {
		    my $sseq1 = shift @sseq;
		    my @sseq2 = @sseq;	    
			while (@sseq2) {
				my $sseq2 = shift @sseq2;
				my @aa1 = split "", $sseq1;
				my @aa2 = split "", $sseq2;
				my $dist;
				while (@aa1) {
				    my $aa1 = shift @aa1;
				    my $aa2 = shift @aa2;
				    next if $aa1 eq $aa2;
				    my $penalty;
				    my $gapp = 3;
				    if ($aa1 ne '-' && $aa2 ne '-') {
					$penalty = abs($matrix{$aa1}{$aa2});
					$gapp = 3;
				    }else{
					$penalty = $gapp;
					$gapp++;
				    }
				    $dist += $penalty;
				}
				if (!$length) {
				    my $wow;
				}
                
				$dist /= $length;
				if ($min_dist < $dist) {
				    $min_dist = $dist;
				}
			}
		}
		my $dist = 1;
		while (@sseq_tmp) {
		   my @aa1 = split "", $sseq;
		   my @aa2 = split "", shift @sseq_tmp;
		   my $tmp = 0;
		    while (@aa1) {
			my $aa1 = shift @aa1;
			my $aa2 = shift @aa2;
			next if $aa1 eq $aa2;
			my $penalty;
			my $gapp = 3;
			if ($aa1 ne '-' && $aa2 ne '-') {
			    $penalty = abs($matrix{$aa1}{$aa2});
			    $gapp = 3;
			}else{
			    $penalty = $gapp;
			    $gapp++;
			}
			$tmp += $penalty;
		    }
		    $tmp /= $length;
		    if ($tmp<$dist) {
			$dist = $tmp;
		    }
		}
		if ($dist<=$min_dist) {
		    print OUT ">" . $key . "\n" . join("\n",unpack("(A60)*",$seqs{$key})) . "\n";
		    $num_seq++;
		}else{
		    my $wow;
		}
	}
	close OUT;
	return($num_seq);
}

sub multiregex{
    my ($toto,@cucu) = @_;
    my $result = 0;
    while (@cucu) {
        my $cucu = shift @cucu;
        if ($toto=~/$cucu/) {
            $result = 1;
            undef @cucu;
        }
    }
    return $result;
}
sub parser_fasta{
    my ($in) = @_;
    my %out;
    open IN, $in;
    my $name;
    while(<IN>){
        chomp;
        if(/>(\S+)/){
            $name = $1;
        }else{
            $out{$name} .= $_;
        }
    }
    close IN;
    return %out;
}
sub matrix_extractor{
    my ($f) = @_;
    open(F, $f);
    my @pos;
    my %m;
    while (<F>) {
        chomp;
        next if /^#/;
        if (/^\s/) {
            @pos = split " ", $_;
            #shift @pos;
        }else{
            my @line = split " ", $_;
            my $aa = shift @line;
	    next if $aa eq '*';
            for(my $i = 0;$i<scalar @line; $i++){
                $m{$pos[$i]}{$aa} = $line[$i];
            }
        }
        
    }
    return %m;
}

sub get_leaf_nodes{
   my ($file) = @_;
   open(FILE, $file);
   my $tree = <FILE>;
   my @tree = split /\:/, $tree;
   $_=~s/\(//g for @tree;
   $_=~s/\)//g for @tree;
   my @output;
   foreach(@tree){
	if (/[\d\.\e\-]+\,(\S+)/) {
		push @output, $1;
	}elsif(/(\S+),[\d\.\e\-]+/){
		push @output, $1;
	}elsif(/^[\d\.\e\-]+$/){
		
	}else{
		push @output, $_;
	}
	
   }
   return(@output);
}

sub DefineModel{
	my ($fasta) = @_;
	my %seqs = parser_fasta($fasta);
	my @keys = keys %seqs;
	my $key = shift @keys;
	my $seq = $seqs{$key};
	my $model;
	if ($seq=~/[GALMFWKQESPVICYHRNDT]+/i) {
		$model = 'PROTGAMMALG';
	}elsif($seq=~/[ATCGN]/i){
		$model = 'GTRGAMMAI';
	}
	return $model;
}
1;