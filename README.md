# Core Genome Evolutionary Placement v.1

## Introduction
This pipeline is intended to allow the user to place metagenomic fragments in a Core-Genome 
based phylogenetic tree, to identify known and unknown species belonging to a defined phylogenetic 
clade. This will allow the user to understand the environmental distribution of the clade of interest.

The pipeline is basically written in perl.

## How does it work?
It requires an important number of pre-computed datasets: 
- A Core Genome built with 3 clades and its Phylogenetic reconstruction:
    - The clade of interest (Query)
    - Two clades, outgroups of the clade of interest (Close and Distant).

- Each core-gene alignment has to be divided by clade, so we can distinguish between the clade of interest and the outgroups. *The core genes for the three clades must be formated as HMMer database*
- A list of reference sequences
- A folder with the metagenomic datasets

## Help Display
    usage: /Volumes/biggem/Users/mgarcia/data/Acinetobacter_postanalysis/pipeline/CoreGenomeEPA_PIPELINE.pl [options]
    -h this message
    -m input metagenomic dataset directory (REQUIRED)
    -u Query Core Genome HMMsearch database (REQUIRED)
    -d Close OUTGROUP HMMsearch database NAME or PATH (REQUIRED)
	    if the option NAME is used, the database is expected to
	    be placed in the same folder as the query database.

    -a Core-gene Alignment FOLDER (REQUIRED)
	    Alignments must contain the sequences from all three 
	    core genomes named by the lineages

     -t Distant OUTGROUP HMMsearch database NAME or PATH (REQUIRED)
	    if the option NAME is used, the database is expected to
    	be placed in the same folder as the query database.

     -o Output DIRECTORY or NAME where construct the output (REQUIRED)
     -p Core Genome Phylogenetic tree (REQUIRED)
     -s <value<=2> Minimum normalized score (REQUIRED)
     -r <value<=2> Minimum Query/Outgroup ratio (REQUIRED)
     -l Accepted List of References (Optional: Default All references)

## Algorithm
The pipeline is divided in 4 phases:
- Phase 1: Check whether all required data are present and the creation of a work directory with all important data
- Phase 2: Search of Metagenomic data against the three core genomes and selection of significant hits
- Phase 3: Re-alignment of significant metagenomic data against the Core Genes of the interesting clade, and removal of distant hits.
- Phase 4: Evolutionary placement in the Phylogenetic tree of the clade of interest

The pipeline has specific requirements in terms of software. They need to be present and accessible in the user's computer
- HMMer
- RAxML (v. 8 or higher)
- Perl Dependencies (*libraries that need to be installed*):
    - Scalar::Util
    - File::chdir

